/*
 * Viide: http://www.kosbie.net/cmu/fall-09/15-110/handouts/recursion/Cryptarithms.java
 */

import java.util.HashMap;
import java.util.HashSet;

public class Puzzle {

   private static String letters;
   private static String[] words;
   private static long SolutionCounter = 0;

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {
      //cryptArithm("SEND", "MORE", "MONEY");
      //cryptArithm("IDAHO","NEVADA", "STATES");
      //cryptArithm("SIENNA", "SILVER", "COLORS");
      //cryptArithm("RENOIR", "SEURAT", "GAUGUIN");


      cryptArithm(args[0], args[1], args[2]);
      if(words.length != 3) {
         throw new RuntimeException("You need to have 3 parameters!");
      }
      for(String s: words){
         if(s.length() > 18){
            throw new RuntimeException("Must not exceed 18 characters!" + s);
         }
      }
   }

   public static void permutation(int n, int k){
      permutation (n, k, new HashSet<Integer>(), new int[k]);
   }

   public static void permutation (int n, int k, HashSet<Integer> set, int[] permutation){
      if(set.size() == k) {
         doPermutation(n, k, set, permutation);
      }
      else {
         for(int i = 0; i < n; i++){
            if(!set.contains(i)){
               permutation[set.size()] = i;
               set.add(i);
               permutation(n,k,set,permutation);
               set.remove(i);
            }
         }
      }
   }

   public static void doPermutation(int n, int k, HashSet<Integer> set, int[] permutation){
      HashMap<Character, Integer> charmap = new HashMap<Character, Integer>();
      for (int i = 0; i < k; i++){
         charmap.put(letters.charAt(i),
                 permutation[i]);
      }
      long[] values = new long[3];
      for(int j=0; j<3; j++){
         String word = words[j];
         if(charmap.get(word.charAt(0)) == 0){
            return;
         }
         long val = 0;
         for(int i = 0; i <word.length(); i++){
            val = 10* val + charmap.get(word.charAt(i));
         }
         values[j] = val;
      }
      if (values[0] + values[1] == values[2]){
         SolutionCounter++;
         System.out.println(charmap);
         System.out.format("%8d\n+%7d\n=%7d\n", values[0], values[1], values[2]);
         System.out.println("There are " + SolutionCounter + " solutions.");
      }
   }

   public static void cryptArithm(String s1, String s2, String s3){
      SolutionCounter = 0;
      words= new String[3];
      words[0]= s1;
      System.out.println(s1);
      words[1] = s2;
      System.out.println(s2);
      words[2] = s3;
      System.out.println(s3);
      String all = s1 + s2 + s3;
      letters = "";
      for(int i = 0; i < all.length(); i++){
         char c = all.charAt(i);
         if(letters.indexOf(c) < 0){
            letters += c;
         }
      }
      permutation(10, letters.length());
   }

}

